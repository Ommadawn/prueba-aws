//-----------------------------------------------------------------------------
// Document   : DAOFactory
// Created on : 11-may-2019, 11:26:24
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.dao;

import com.pruebaaws.aplicacion.dao.interfaces.IDAOFactory;

/**
 *
 * @author Ivens Huertas
 */
public class DAOFactory implements IDAOFactory {

	@Override
	public ProductoDAO getProductoDAO() {
		return new ProductoDAO();
	}
}
