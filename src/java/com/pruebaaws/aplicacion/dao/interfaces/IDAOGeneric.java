//-----------------------------------------------------------------------------
// Document   : IDAOGeneric
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.dao.interfaces;

import com.pruebaaws.aplicacion.dao.utils.BDException;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Ivens Huertas
 * @param <T>  Tipo de objeto a utilizar
 * @param <ID> Tipo de identificador usado
 */
public interface IDAOGeneric<T, ID extends Serializable> {

	/**
	 * Almacena el objeto en la BD
	 *
	 * @param obj Objeto a almacernar en la BD
	 *
	 * @return int ID del objeto guardado
	 *
	 * @throws BDException
	 */
	public abstract ID save(T obj) throws BDException;

	/**
	 * Actualiza el objeto en la BD
	 *
	 * @param obj Objeto a actualizar en la BD
	 *
	 * @throws BDException
	 */
	public abstract void update(T obj) throws BDException;

	/**
	 * Elimina el objeto de la BD
	 *
	 * @param id Identificador del objeto a eliminar en la BD
	 *
	 * @return boolean ¿se ha eliminado el objeto?
	 *
	 * @throws BDException
	 */
	public abstract boolean delete(ID id) throws BDException;

	/**
	 * Devuelve un objeto de tipo T a partir de su ID
	 *
	 * @param id Identificador del objeto a devolver desde la BD
	 *
	 * @return Entidad de tipo T
	 *
	 * @throws BDException
	 */
	public abstract T getById(ID id) throws BDException;

	/**
	 * Devuelve una lista con todos los elementos de una entidad
	 *
	 * @param first
	 * @param size
	 *
	 * @return List Lista de tipo T
	 *
	 * @throws BDException
	 */
	public abstract List<T> getAll(int first, int size) throws BDException;

}
