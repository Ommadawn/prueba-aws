//-----------------------------------------------------------------------------
// Document   : IDAOFactory
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.dao.interfaces;

import com.pruebaaws.aplicacion.dao.ProductoDAO;


/**
 *
 * @author Ivens Huertas
 */
public interface IDAOFactory {

	/**
	 * Devuelve un DAO de Comment
	 *
	 * @return
	 */
	public abstract ProductoDAO getProductoDAO();

}
