//-----------------------------------------------------------------------------
// Document   : IProductoDAO
// Created on : 23-abr-2019, 20:36:04
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.dao.interfaces;

import com.pruebaaws.aplicacion.en.Producto;

/**
 *
 * @author Ivens Huertas
 */
public interface IProductoDAO extends IDAOGeneric<Producto, Integer> {

}
