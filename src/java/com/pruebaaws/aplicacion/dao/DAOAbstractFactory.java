//-----------------------------------------------------------------------------
// Document   : DAOAbstractFactory
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.dao;

import com.pruebaaws.aplicacion.dao.interfaces.IDAOFactory;

/**
 *
 * @author Ivens Huertas
 */
public abstract class DAOAbstractFactory implements IDAOFactory {

	private static DAOFactory instance = null;

	//-------------------------------------------------------------------------
	// getInstance --> Patrón Singlenton
	//-------------------------------------------------------------------------
	public static DAOFactory getInstance() {
		if(instance == null) {
			instance = new DAOFactory();
		}
		return instance;
	}

}
