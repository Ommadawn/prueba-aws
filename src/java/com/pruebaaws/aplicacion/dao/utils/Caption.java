//-----------------------------------------------------------------------------
// Document   : Caption
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.dao.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Ivens Huertas
 * Añade una nueva anotación a cada propiedad que incluya un nombre amigable de
 * dicha propiedad
 * 
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Caption {

	String value();
}
