//-----------------------------------------------------------------------------
// Document   : BDException
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.dao.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.validation.Path;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

/**
 *
 * @author Ivens Huertas
 */
public class BDException extends Exception {

	/**
	 * Un TreeSet donde guardar todos los BDMessage.
	 * Se usa un TreeSet para que los mensajes salgan ordenados por orden alfabético.
	 */
	private final Set<BDMessage> bussinessMessages = new TreeSet<>();

	/**
	 * Constructor al que directamente se le pasa una lista de BDMessage.
	 * Esto permitirá generar mensajes al usuario aunque no haya habido ninguna excepción.
	 *
	 * @param bussinessMessages
	 */
	public BDException(List<BDMessage> bussinessMessages) {
		this.bussinessMessages.addAll(bussinessMessages);
	}

	/**
	 * Constructor al que directamente se le pasa un único BDMessage.
	 * Esto permitirá generar mensajes al usuario aunque no haya habido ninguna excepción.
	 *
	 * @param bussinessMessage
	 */
	public BDException(BDMessage bussinessMessage) {
		this.bussinessMessages.add(bussinessMessage);
	}

	/**
	 * Constructor al que se le pasa una Exception.
	 * Eso permite mostrar al usuario un mensaje aunque sea un Exception.
	 *
	 * @param ex
	 */
	public BDException(Exception ex) {
		bussinessMessages.add(new BDMessage(null, ex.toString()));
	}

	/**
	 * Constructor al que se le pasa una javax.validation.ConstraintViolationException.
	 * Este constructor creará un BDMessage por cada uno de los javax.validation.ConstraintViolation.
	 *
	 * @param cve
	 */
	public BDException(javax.validation.ConstraintViolationException cve) {
		cve.getConstraintViolations().stream().forEach((constraintViolation) -> {
			String fieldName;
			String message;

			fieldName = getCaptions(constraintViolation.getRootBeanClass(), constraintViolation.getPropertyPath());
			message = constraintViolation.getMessage();

			bussinessMessages.add(new BDMessage(fieldName, message));
		});
	}

	/**
	 * Constructor al que se le pasa una org.hibernate.exception.ConstraintViolationException.
	 * Creará un único BDMessage en función del mensaje de la excepción.
	 *
	 * @param cve
	 */
	public BDException(org.hibernate.exception.ConstraintViolationException cve) {
		bussinessMessages.add(new BDMessage(null, cve.getLocalizedMessage()));
	}

	/**
	 * Retorna la lista de todos los BDMessage.
	 *
	 * @return TreeSet con todos los BDMessage
	 */
	public Set<BDMessage> getBussinessMessages() {
		return bussinessMessages;
	}

	/**
	 * En función de una clase de dominio y el nombre de la columna nos retorna el caption
	 *
	 * @param clazz
	 * @param path
	 *
	 * @return Caption correspondiente
	 */
	private String getCaptions(Class clazz, Path path) {
		StringBuilder sb = new StringBuilder();
		if(path != null) {
			Class currentClazz = clazz;
			for(Path.Node node: path) {
				ClassAndCaption clazzAndCaption = getSingleCaption(currentClazz, node.getName());
				if(clazzAndCaption.caption != null) {
					if(sb.length() != 0) {
						sb.append(".");
					}
					if(node.isInIterable()) {
						if(node.getIndex() != null) {
							sb.append(node.getIndex());
							sb.append("º ");
							sb.append(clazzAndCaption.caption);
						}
						else if(node.getKey() != null) {
							sb.append(clazzAndCaption.caption);
							sb.append(" de ");
							sb.append(node.getKey());
						}
						else {
							sb.append(clazzAndCaption.caption);
						}
					}
					else {
						sb.append(clazzAndCaption.caption);
					}
				}
				else {
					sb.append("");
				}
				currentClazz = clazzAndCaption.clazz;
			}

			return sb.toString();

		}
		else {
			return null;
		}

	}

	/**
	 * Retorna el tipo y el caption de un campo, tanto si dicha anotación
	 * está en una propiedad Java o en un método Java
	 *
	 * @param clazz
	 * @param fieldName
	 *
	 * @return tipo y caption de un campo
	 */
	private ClassAndCaption getSingleCaption(Class clazz, String fieldName) {
		ClassAndCaption clazzAndCaptionField;
		ClassAndCaption clazzAndCaptionMethod;

		if((fieldName == null) || (fieldName.trim().equals(""))) {
			return new ClassAndCaption(clazz, null);
		}

		clazzAndCaptionField = getFieldCaption(clazz, fieldName);
		if((clazzAndCaptionField != null) && (clazzAndCaptionField.caption != null)) {
			return clazzAndCaptionField;
		}

		clazzAndCaptionMethod = getMethodCaption(clazz, fieldName);
		if((clazzAndCaptionMethod != null) && (clazzAndCaptionMethod.caption != null)) {
			return clazzAndCaptionMethod;
		}

		if(clazzAndCaptionField != null) {
			return new ClassAndCaption(clazzAndCaptionField.clazz, fieldName);
		}
		else if(clazzAndCaptionMethod != null) {
			return new ClassAndCaption(clazzAndCaptionMethod.clazz, fieldName);
		}
		else {
			return new ClassAndCaption(clazz, fieldName);
		}
	}

	/**
	 * Si una propiedad tiene la anotación @Caption, retorna el tipo y el valor
	 * del caption y, si no, retornará el tipo y null
	 *
	 * @param clazz
	 * @param fieldName
	 *
	 * @return el tipo y valor (o null) de la propiedad pasada
	 */
	private ClassAndCaption getFieldCaption(Class clazz, String fieldName) {
		Field field = ReflectionUtils.findField(clazz, fieldName);
		if(field == null) {
			return null;
		}

		Caption caption = field.getAnnotation(Caption.class);
		if(caption != null) {
			return new ClassAndCaption(field.getType(), caption.value());
		}
		else {
			return new ClassAndCaption(field.getType(), null);
		}
	}

	/**
	 * Si un método tiene la anotación @Caption, retorna el tipo y el valor
	 * del caption y sino retornará el tipo y null.
	 *
	 * @param clazz
	 * @param methodName
	 *
	 * @return el tipo y valor (o null) del método pasado
	 */
	private ClassAndCaption getMethodCaption(Class clazz, String methodName) {
		String suffixMethodName = StringUtils.capitalize(methodName);
		Method method = ReflectionUtils.findMethod(clazz, "get" + suffixMethodName);
		if(method == null) {
			method = ReflectionUtils.findMethod(clazz, "is" + suffixMethodName);
			if(method == null) {
				return null;
			}
		}

		Caption caption = method.getAnnotation(Caption.class);
		if(caption != null) {
			return new ClassAndCaption(method.getReturnType(), caption.value());
		}
		else {
			return new ClassAndCaption(method.getReturnType(), null);
		}

	}

	private class ClassAndCaption {

		Class clazz;
		String caption;

		public ClassAndCaption(Class clazz, String caption) {
			this.clazz = clazz;
			this.caption = caption;
		}
	}
}
