//-----------------------------------------------------------------------------
// Document   : ProductoDAO
// Created on : 11-may-2019, 11:25:56
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.dao;

import com.pruebaaws.aplicacion.dao.interfaces.IProductoDAO;
import com.pruebaaws.aplicacion.en.Producto;

/**
 *
 * @author Ivens Huertas
 */
public class ProductoDAO extends DAOGeneric<Producto, Integer> implements IProductoDAO {

}
