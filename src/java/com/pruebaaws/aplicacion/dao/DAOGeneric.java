//-----------------------------------------------------------------------------
// Document   : DAOGeneric
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.dao;

import com.pruebaaws.aplicacion.dao.utils.BDException;
import com.pruebaaws.aplicacion.dao.utils.BDMessage;
import com.pruebaaws.aplicacion.hibernate.HibernateUtil;
import com.pruebaaws.aplicacion.dao.interfaces.IDAOGeneric;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ivens Huertas
 * @param <T>  Tipo de objeto a utilizar
 * @param <ID> Tipo de identificador de la clase
 */
public class DAOGeneric<T, ID extends Serializable> implements IDAOGeneric<T, ID> {

	SessionFactory sessionFactory;
	private final Class<T> myClass;
	private static final Logger LOGGER = LogManager.getLogger();

	/**
	 * Obtiene el nombre del método actual (util para LOGGER)
	 *
	 * @return
	 */
	private static String getMethodName() {
		return Thread.currentThread().getStackTrace()[2].getMethodName();
	}

	public DAOGeneric() {
		sessionFactory = HibernateUtil.getSessionFactory();
		this.myClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	//---------------------------------------------------------------------------------------------
	// save
	//---------------------------------------------------------------------------------------------
	@Override
	public ID save(T obj) throws BDException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.getTransaction();

		try {
			tx.begin();
			ID id = (ID) session.save(obj);
			tx.commit();
			return id;
		}
		// <editor-fold defaultstate="collapsed" desc="Excepciones">
		catch(javax.validation.ConstraintViolationException | org.hibernate.exception.ConstraintViolationException cve) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new BDException(cve);
		}
		catch(RuntimeException ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw ex;
		}
		catch(Exception ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new RuntimeException(ex);
		}
		// </editor-fold>
	}

	//---------------------------------------------------------------------------------------------
	// update
	//---------------------------------------------------------------------------------------------
	@Override
	public void update(T obj) throws BDException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.getTransaction();

		try {
			tx.begin();
			session.update(obj);
			tx.commit();
		}
		// <editor-fold defaultstate="collapsed" desc="Excepciones">
		catch(javax.validation.ConstraintViolationException | org.hibernate.exception.ConstraintViolationException cve) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new BDException(cve);
		}
		catch(RuntimeException ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw ex;
		}
		catch(Exception ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new RuntimeException(ex);
		}
		//</editor-fold>
	}

	//---------------------------------------------------------------------------------------------
	// delete
	//---------------------------------------------------------------------------------------------
	@Override
	public boolean delete(ID id) throws BDException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.getTransaction();

		try {
			tx.begin();
			T entity = (T) session.get(getEntityClass(), id);

			if(entity == null) {
				throw new BDException(new BDMessage(null, "Los datos a borrar ya no existen"));
			}
			else {
				session.delete(entity);
				tx.commit();
				return true;
			}
		}
		// <editor-fold defaultstate="collapsed" desc="Excepciones">
		catch(javax.validation.ConstraintViolationException | org.hibernate.exception.ConstraintViolationException cve) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new BDException(cve);
		}
		catch(BDException | RuntimeException ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw ex;
		}
		catch(Exception ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new RuntimeException(ex);
		}
		//</editor-fold>
	}

	//---------------------------------------------------------------------------------------------
	// getById
	//---------------------------------------------------------------------------------------------
	@Override
	public T getById(ID id) throws BDException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.getTransaction();

		try {
			tx.begin();
			T entity = (T) session.get(myClass, id);
			tx.commit();
			return entity;
		}
		// <editor-fold defaultstate="collapsed" desc="Excepciones">
		catch(javax.validation.ConstraintViolationException | org.hibernate.exception.ConstraintViolationException cve) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new BDException(cve);
		}
		catch(RuntimeException ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw ex;
		}
		catch(Exception ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new RuntimeException(ex);
		}
		//</editor-fold>
	}

	//---------------------------------------------------------------------------------------------
	// getAll
	//---------------------------------------------------------------------------------------------
	@Override
	public List<T> getAll(int first, int size) throws BDException {
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.getTransaction();

		try {
			tx.begin();
			Query query = session.createQuery("SELECT e FROM " + getEntityClass().getName() + " e");
			query.setFirstResult(first);
			query.setMaxResults(size);
			List<T> entities = query.getResultList();
			tx.commit();
			return entities;
		}
		// <editor-fold defaultstate="collapsed" desc="Excepciones">
		catch(javax.validation.ConstraintViolationException | org.hibernate.exception.ConstraintViolationException cve) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new BDException(cve);
		}
		catch(RuntimeException ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw ex;
		}
		catch(Exception ex) {
			try {
				if(tx.getStatus() == TransactionStatus.ACTIVE) {
					tx.rollback();
				}
			}
			catch(Exception exc) {
				LOGGER.error("Error rollback in method='" + getMethodName() + "'");
			}
			throw new RuntimeException(ex);
		}
		//</editor-fold>
	}

	/**
	 * Devuelve el tipo de clase parametrizada que se ha pasado
	 *
	 * @return Class<T>
	 */
	private Class<T> getEntityClass() {
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

}
