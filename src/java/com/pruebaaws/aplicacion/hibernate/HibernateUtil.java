//-----------------------------------------------------------------------------
// Document   : Comment
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.context.internal.ThreadLocalSessionContext;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 *
 * @author Ivens Huertas
 */
public class HibernateUtil {

	private static SessionFactory sessionFactory;

	/**
	 * -------------------------------------------------------------------------
	 * Factoría del objeto SessionFactory
	 * -------------------------------------------------------------------------
	 */
	public static synchronized void buildSessionFactory() {
		if(sessionFactory == null) {
			Configuration conf = new Configuration().configure().setProperty("hibernate.current_session_context_class", "thread");

			// Aquí ponemos los EN que queramos que aparezcan en la BD
			conf.addAnnotatedClass(com.pruebaaws.aplicacion.en.Producto.class);

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
													.applySettings(conf.getProperties())
													.configure("hibernate.cfg.xml")
													.build();

			sessionFactory = conf.buildSessionFactory(serviceRegistry);			
		}
	}

	/**
	 * -------------------------------------------------------------------------
	 * Crea la nueva sesión
	 * -------------------------------------------------------------------------
	 */
	public static void openSessionAndBindToThread() {
		Session session = sessionFactory.openSession();
		ThreadLocalSessionContext.bind(session);
	}

	/**
	 * -------------------------------------------------------------------------
	 * Retorna el objeto estático SessionFactory (patrón singleton)
	 * -------------------------------------------------------------------------
	 *
	 * @return SessionFactory
	 */
	public static SessionFactory getSessionFactory() {
		if(sessionFactory == null) {
			buildSessionFactory();
		}
		return sessionFactory;
	}

	/**
	 * -------------------------------------------------------------------------
	 * Cierra la sesión
	 * -------------------------------------------------------------------------
	 */
	public static void closeSessionAndUnbindFromThread() {
		Session session = ThreadLocalSessionContext.unbind(sessionFactory);
		if(session != null) {
			session.close();
		}
	}

	/**
	 * -------------------------------------------------------------------------
	 * Finaliza el objeto SessionFactory
	 * -------------------------------------------------------------------------
	 */
	public static void closeSessionFactory() {
		if((sessionFactory != null) && (sessionFactory.isClosed() == false)) {
			sessionFactory.close();
		}
	}
}
