//-----------------------------------------------------------------------------
// Document   : SaveProductosServlet
// Created on : 11-may-2019, 13:15:21
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.controller;

import com.pruebaaws.aplicacion.dao.DAOAbstractFactory;
import com.pruebaaws.aplicacion.dao.interfaces.IDAOFactory;
import com.pruebaaws.aplicacion.dao.interfaces.IProductoDAO;
import com.pruebaaws.aplicacion.dao.utils.BDException;
import com.pruebaaws.aplicacion.en.Producto;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ivens Huertas
 */
@WebServlet(name = "SaveProductosServlet", urlPatterns = {"/save"})
public class SaveProductosServlet extends MainServlet {

	private static final long serialVersionUID = 1L;
	private static final IDAOFactory factoria = DAOAbstractFactory.getInstance();
	private static final IProductoDAO productoDAO = factoria.getProductoDAO();
	private static final Logger LOGGER = LogManager.getLogger();
	
	@Override
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		boolean areValidParameters = true;
		String nombre = request.getParameter("nombre");
		String precio = request.getParameter("precio");
		float precioFloat = 0;
		
		if(nombre != null && precio != null && !nombre.equals("")) {
			try {
				precioFloat = Float.parseFloat(precio);
			}
			catch(NumberFormatException e) {
				areValidParameters = false;
			}
			
			// Todo OK!
			if(areValidParameters) {
				Producto p = new Producto(nombre, precioFloat);
				try {
					productoDAO.save(p);
					out.print("ok");
				}
				catch(BDException ex) {
					LOGGER.warn("Error guardando producto " + p.toString() + ex);
					out.print("Error guardando producto en la BD");
				}
			}
			else {
				out.print("Error con el formato del precio");
			}
		}
		else {
			out.print("Parámetros incorrectos o incompletos");
		}
		out.flush();
	}
	
		
	/**
	 * servlet info
	 *
	 * @return
	 */
	@Override
	public String getServletInfo() {
		return "Servlet que almacena un producto en la DB";
	}
	
}
