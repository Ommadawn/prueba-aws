//-----------------------------------------------------------------------------
// Document   : MainServlet
// Created on : 27-abr-2016, 21:51:46
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ivens Huertas
 */
@WebServlet(name = "MainServlet")
public abstract class MainServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// Context
	protected static ServletContext context = null;
	protected static String contextPath = null;

	protected static String errorURL = null;

	/**
	 * Initialize Servlets context parameters (global)
	 */
	@Override
	public void init() {

	}

	/**
	 * To be implemented by each Servlet
	 *
	 * @param request
	 * @param response
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	protected abstract void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}


	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Forwards to a given Servlet name
	 *
	 * @param address
	 * @param request
	 * @param response
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void forwardToServlet(String address, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getNamedDispatcher(address);
		dispatcher.forward(request, response);
	}

	/**
	 * Forwards to a given URL
	 *
	 * @param address
	 * @param request
	 * @param response
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void forwardToURL(String address, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(address);
		dispatcher.forward(request, response);
	}

	/**
	 * Redirects to the given address (context + address given)
	 *
	 * @param address
	 * @param response
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void redirectToURL(String address, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(address);
	}

}
