//-----------------------------------------------------------------------------
// Document   : LoadProductosServlet
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.controller;

import com.pruebaaws.aplicacion.dao.DAOAbstractFactory;
import com.pruebaaws.aplicacion.dao.interfaces.IDAOFactory;
import com.pruebaaws.aplicacion.dao.interfaces.IProductoDAO;
import com.pruebaaws.aplicacion.dao.utils.BDException;
import com.pruebaaws.aplicacion.en.Producto;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ivens Huertas
 */
@WebServlet(name = "LoadProductosServlet", urlPatterns = {"/load"})
public class LoadProductosServlet extends MainServlet {
   
	private static final long serialVersionUID = 1L;
	private static final IDAOFactory factoria = DAOAbstractFactory.getInstance();
	private static final IProductoDAO productoDAO = factoria.getProductoDAO();
	private static final Logger LOGGER = LogManager.getLogger();
	
	@Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		List<Producto> productoList = null;
		try {
			productoList = productoDAO.getAll(0, Integer.MAX_VALUE);
		}
		catch(BDException ex) {
			LOGGER.warn("Error cargando productos. " + ex);
		}
		
		StringBuilder builder = new StringBuilder();
		if(productoList != null && productoList.size() > 0) {
			for(Producto p: productoList) {
				builder.append(p.toString()).
						append("<br/>");
			}
			out.print(builder.toString());
		}
		else {
			out.println("No hay productos a mostrar");
		}
		out.flush();
    } 

			
	/**
	 * servlet info
	 *
	 * @return
	 */
	@Override
	public String getServletInfo() {
		return "Servlet que lee los productos de la DB y los pone en el response";
	}
}
