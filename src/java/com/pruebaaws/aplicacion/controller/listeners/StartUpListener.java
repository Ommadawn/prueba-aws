//-----------------------------------------------------------------------------
// Document   : StartUpListener
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.controller.listeners;

import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

/**
 *
 * @author Ivens Huertas
 */
@WebListener
public class StartUpListener implements ServletContextListener {

	private static final Logger LOGGER = LogManager.getLogger();

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();

		// Contexto
		String contextPath = context.getContextPath();
		if(!contextPath.equals("/")) {
			contextPath += "/";
		}

		String context_name = "PruebaAWS/";
		String base_dir = context.getInitParameter("protocol") + context.getInitParameter("domain") + "/";
		String base_dir_debug = base_dir + context_name;

		// Main directories
		String css_dir = contextPath + context.getInitParameter("css_dir");
		String css_dir_other = contextPath + context.getInitParameter("css_dir_other");
		String js_dir = contextPath + context.getInitParameter("js_dir");
		String js_dir_other = contextPath + context.getInitParameter("js_dir_other");
		String static_dir = contextPath + context.getInitParameter("static_dir");


		// Mapa de nombres de (servlets -> patrón). Automático
		HashMap<String, ? extends ServletRegistration> servletRegisters = (HashMap<String, ? extends ServletRegistration>) context.getServletRegistrations();
		HashMap<String, String> servletPatterns = new HashMap<>();

		for(HashMap.Entry<String, ? extends ServletRegistration> entry : servletRegisters.entrySet()) {
			String key = entry.getKey();
			for(String value: entry.getValue().getMappings()) {
				if(!key.equals("default") && !key.equals("jsp")) {
					servletPatterns.put(key, value);
				}
				break;
			}
		}
		servletRegisters.clear();
		servletRegisters = null;

		//-----------------------------------------------------------------------------------------
		// Init Application context attributes --> Sólo los atributos compuestos
		//-----------------------------------------------------------------------------------------
		context.setAttribute("context_name", context_name);
		context.setAttribute("base_dir", base_dir_debug);  //@TODO: cambiar el valor a "base_dir" en producción, quitando el debug
		context.setAttribute("css_dir", css_dir);
		context.setAttribute("css_dir_other", css_dir_other);
		context.setAttribute("js_dir", js_dir);
		context.setAttribute("js_dir_other", js_dir_other);
		context.setAttribute("static_dir", static_dir);
		
		LOGGER.info("Startup listener loaded...");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();
		System.out.println("* Stopping application in " + context.getContextPath() + " context");
		
		// Evita que se inice Tomcat 2 veces al arrancar
		String delme = sce.getServletContext().getInitParameter("eraseOnExit");
		if(delme != null && delme.length() > 0) {
			File del = new File(delme);
			if(del.exists()) {
				System.out.println("Deleting file " + delme);
				del.delete();
			}
		}
	}

}
