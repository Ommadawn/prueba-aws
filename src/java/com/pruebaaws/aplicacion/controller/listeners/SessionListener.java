//-----------------------------------------------------------------------------
// Document   : SessionListener
// Created on : 13-may-2019, 18:39:25
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.controller.listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Ivens Huertas
 */
public class SessionListener implements HttpSessionListener {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		LOGGER.info("Session created");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		LOGGER.info("Session destroyed");
	}

}
