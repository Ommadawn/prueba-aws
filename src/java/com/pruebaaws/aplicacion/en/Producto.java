//-----------------------------------------------------------------------------
// Document   : Producto
// Created on : 11-may-2019, 10:52:44
// Author     : Ivens Huertas
//-----------------------------------------------------------------------------
package com.pruebaaws.aplicacion.en;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Ivens Huertas
 */
@Entity
@Table(name = "Productos")
public class Producto implements Serializable {

	// Identificador (PK)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", columnDefinition = "INT UNSIGNED")
	private int id;

	// Nombre
	@NotBlank
	@Type(type = "string")
	@Column(name = "nombre", unique = true)
	private String name;

	// Precio
	@Column(name = "precio")
	private float precio;

	//-------------------------------------------------------------------------
	// Constructores
	//-------------------------------------------------------------------------
	public Producto() {
	}

	public Producto(String name, float precio) {
		this.name = name;
		this.precio = precio;
	}

	//-------------------------------------------------------------------------
	// Getters & Setters
	//-------------------------------------------------------------------------
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	//-------------------------------------------------------------------------
	// Hash & Equals
	//-------------------------------------------------------------------------
	@Override
	public int hashCode() {
		int hash = 3;
		hash = 83 * hash + this.id;
		hash = 83 * hash + Objects.hashCode(this.name);
		hash = 83 * hash + Float.floatToIntBits(this.precio);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null) {
			return false;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		final Producto other = (Producto) obj;
		if(this.id != other.id) {
			return false;
		}
		if(Float.floatToIntBits(this.precio) != Float.floatToIntBits(other.precio)) {
			return false;
		}
		return Objects.equals(this.name, other.name);
	}

	//-------------------------------------------------------------------------
	// toString
	//-------------------------------------------------------------------------
	@Override
	public String toString() {
		return "id=" + id + " { name=\"" + name + "\", precio=" + precio + "€ }";
	}

}
