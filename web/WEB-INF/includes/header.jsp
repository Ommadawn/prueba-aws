<% request.setCharacterEncoding("UTF-8"); %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${applicationScope['css_dir_other']}bootstrap.min.css" rel="stylesheet" media="screen">
<link href="${applicationScope['css_dir']}notify.min.css" rel="stylesheet" media="screen">
<link href="${applicationScope['css_dir']}main.min.css" rel="stylesheet" media="screen">