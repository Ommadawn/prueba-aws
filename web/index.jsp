<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Prueba AWS</title>
		<%@include file="/WEB-INF/includes/header.jsp"%>
    </head>
    <body>
		<div class="container">
			<div>
				<img src="${initParam['static_dir']}logo.png" class="img-center" alt="IESMN Logo"/>
			</div>
			<h1>Productos</h1>
			<hr/>
			<div class="row">
				<div class="col-lg-3 text-center">
					<form>
						<div class="form-group">
							<label for="nombre">Nombre:</label>
							<input type="text" class="form-control" id="nombre" name="nombre" required>
						</div>
						<div class="form-group">
							<label for="precio">Precio:</label>
							<input type="number" class="form-control" id="precio" name="precio" style="text-align: right;" required>
						</div>
						<button type="button" id="btn_save" class="btn btn-danger">Guardar Producto</button>
					</form>
					<br/>
				</div>
				<div class="col-lg-9 text-center">
					<button id="btn_load" type="button" class="btn btn-primary">Cargar productos</button>
					<button id="btn_clear" type="button" class="btn btn-success" disabled>Limpiar productos</button>
					<br/>
					<div class="well well-lg" id="listado"></div>
				</div>
			</div>
		</div>
		<div id="snackbar"></div>
		<%@include file="/WEB-INF/includes/footer.jsp"%>
    </body>
</html>