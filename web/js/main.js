$(document).ready(function () {
	var btnLoad = $('#btn_load');
	var btnSave = $('#btn_save');
	var btnClear = $('#btn_clear');
	var listado = $('#listado');
	var nombre = $('#nombre');
	var precio = $('#precio');
			
	// Cargar productos
	btnLoad.click(function() {
		$.post('/PruebaAWS/load', 
		function (responseText) {
			listado.html(responseText);
			btnClear.prop('disabled', false);
		});
	});
	
	// Guardar productos
	btnSave.click(function() {
		$.post('/PruebaAWS/save', {
			nombre: nombre.val(),
			precio: precio.val()
		}, 
		function(responseText) {
			if(responseText === "ok") {
				notify('Producto insertado con éxito', 'success');
				nombre.val("");
				precio.val("");
			}
			else {
				notify(responseText, 'danger');
			}			
		});
	});
	
	// Limpiar productos
	btnClear.click(function() {
		listado.html("");
		btnClear.prop('disabled', true);
	});
	
	// Notify
	function notify(msg, type) {
		setTimeout(function () {
			showNotify(msg, type);
		}, 500);
	}
	function showNotify(msg, type) {
		var x = document.getElementById('snackbar');
		x.innerHTML = msg;
		x.className = type + " show";

		setTimeout(function () {
			x.className = x.className.replace(" show", "");
			x.innerHTML = '';
		}, 2500);
	}
	
});

